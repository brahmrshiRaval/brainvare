import React, { Component } from "react";
import ServiceCard from "./ServiceItem";
import { Row, Col, Container } from "reactstrap";
import { connect } from "react-redux";
import { getParamsFromQuery } from "../../utils/UrlUtils";

class Service extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      selectedCar: this.props.car,
      isDoorStepServices:
        getParamsFromQuery(this.props.location.search, "isDoorStepServices") ||
        0,
      listCategory: [],
      selectedCategory: {},
      listSubCategory: [],
      selectedSubCategory: {},
      listServices: [],
      listPackages: [],
      servicepoint: [],
      cartlist: this.props.cart,
      newprice: 0,
      isModel: false,
      isCartValidationModelVisible: false,
      isCartHasDoorStepService: false,
      tempCartItem: null,
      packagedetails: false,
      packageListdata: [],
    };
  }

  componentWillMount() {
    if ("scrollRestoration" in window.history) {
      window.history.scrollRestoration = "manual";
    }
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <React.Fragment>
        <main className="main">
          <Container className="mb-5 mt-2">
            <Row className="">
              <Col>
                {this.state.listServices.map((item, index) => (
                  <div className="p-3">
                    <ServiceCard
                      item={item}
                      handleAddcart={(data) => {
                        this.addCartitem(data);
                      }}
                      getServicePrice={(data) => {
                        this.getServicePrice(data);
                      }}
                      isInCart={this.checkIfALreadyExiestInCart(
                        item.service_name
                      )}
                      onClickDelete={(name) => {
                        this.onDeleteItem(name);
                      }}
                      setServiceList={(data) => {
                        this.setServiceList(data);
                      }}
                      selectedCar={this.state.selectedCar}
                      handleAddCar={() => {
                        this.handleAddCar();
                      }}
                    />
                  </div>
                ))}
              </Col>
            </Row>
          </Container>
        </main>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  cart: state.form.Cart.cart,
});
const mapDispatchToProps = (dispatch) => {
  return {
    // dispatching plain actions
    deleteCartdata: (data) => dispatch({ type: "DELETE_ITEM", payload: data }),
    addCartdata: (data) => dispatch({ type: "ADD_CART", payload: data }),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Service);
