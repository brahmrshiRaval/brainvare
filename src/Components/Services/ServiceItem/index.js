import React, { useState } from "react";
import {
  Card,
  CardBody,
  CardTitle,
  CardSubtitle,
  CardFooter,
} from "reactstrap";
import Button from "@material-ui/core/Button";
import DeleteIcon from "@material-ui/icons/Delete";
import "../ScrollableList/stylesheet.scss";

const ServiceCard = (props) => {
  return (
    <Card
      className="shadow service-card "
      style={{ overflow: "hidden", height: "100% " }}
    >
      <div
        className="card-background"
        style={{
          backgroundImage: `url(${props.item.imageurls[0]})`,
        }}
      ></div>
      <CardBody className="text-center">
        <CardTitle className="font-weight-bold" style={{ fontSize: "18px" }}>
          {props.item.service_name}
        </CardTitle>

        {props.item.service_discount.amount !== "" ? (
          <CardSubtitle className="d-inline-flex">
            <h5>
              {" "}
              <span
                className="disscount-price"
                style={{
                  fontSize: "0.875rem",
                  fontWeight: "normal",
                  color: "#a2a0a0",
                }}
              >
                {" "}
                {/*&#x20b9; */}
                Rs.{props.item.service_price}
              </span>
            </h5>
            <h5 style={{ fontSize: "1.25rem", display: "inline-flex" }}>
              {" "}
              &#x20b9;
              {props.item.service_price - props.item.service_discount.amount}
            </h5>
          </CardSubtitle>
        ) : (
          <CardSubtitle>
            <h5 style={{ fontSize: ".9375rem", display: "inline-flex" }}>
              {" "}
              &#x20b9;
              {props.item.service_price}
            </h5>
          </CardSubtitle>
        )}
        <div
          style={{ maxHeight: "70px", minHeight: "70px", marginBottom: "12x" }}
          className=""
        >
          <p style={{ fontSize: "1.25rem" }}>
            {props.item.service_description_second
              .split("\n")
              .map((str, index) => (
                <span key={index} className="">
                  {str}
                </span>
              ))}
          </p>
        </div>
        <div>
          {props.selectedCar?.vehicle && (
            <div style={{ marginTop: "7px" }} className="removecar">
              {props.isInCart ? (
                <Button
                  variant="contained"
                  color="secondary"
                  // className="theme-font d-inliine-flex"
                  className="theme-font "
                  startIcon={<DeleteIcon />}
                  onClick={() => {
                    props.onClickDelete(props.item.service_name);
                  }}
                >
                  REMOVE FROM CART
                </Button>
              ) : (
                <Button
                  variant="outlined"
                  color="secondary"
                  className="theme-font "
                  onClick={() => {
                    props.handleAddcart(props.item, false);
                  }}
                >
                  ADD TO CART
                </Button>
              )}
            </div>
          )}
        </div>
      </CardBody>
      <CardFooter
        style={{
          backgroundColor: "white",
          minHeight: "170px",
          maxHeight: "170px",
        }}
        className="mt-20"
      ></CardFooter>
    </Card>
  );
};

export default ServiceCard;
