import React, { Component, Fragment } from "react";
import { Helmet } from "react-helmet";
import { Container } from "reactstrap";
export default class index extends Component {
  scrollPositions = {};
  componentWillMount() {
    if ("scrollRestoration" in window.history) {
      window.history.scrollRestoration = "manual";
    }
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <Fragment>
        {" "}
        <Helmet>
          <title></title>
          <meta name="description" content="" />
          <meta property="og:title" content="" />
          <meta property="og:description" content="" />
          <meta name="twitter:description" content="" />
          <meta name="twitter:title" content="" />
          <link rel="canonical" href="" />
        </Helmet>
      </Fragment>
    );
  }
}
