import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import Slider from "react-slick";
import "./slider.scss";
export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [
        "http://justadomain.xyz/demo/modello/images/brands04.png",
        "http://justadomain.xyz/demo/modello/images/brands01.png",
        "http://justadomain.xyz/demo/modello/images/brands03.png",
        "http://justadomain.xyz/demo/modello/images/brands05.png",
        "http://justadomain.xyz/demo/modello/images/brands01.png",
        "http://justadomain.xyz/demo/modello/images/brands04.png",
        "http://justadomain.xyz/demo/modello/images/brands01.png",
        "http://justadomain.xyz/demo/modello/images/brands03.png",
        "http://justadomain.xyz/demo/modello/images/brands05.png",
        "http://justadomain.xyz/demo/modello/images/brands01.png",
      ],
    };
  }

  render() {
    var settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 5,
      slidesToScroll: 1,
      autoplaySpeed: 5000,
      autoplay: true,
      lazyLoad: true,
      arrows: true,
      className: "slides",
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 5,
            slidesToScroll: 1,
            infinite: true,
            dots: true,
          },
        },
        {
          breakpoint: 780,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    };

    return (
      <div className="mt-5">
        <Container>
          <Row className="content-bg">
            <Col>
              <Slider {...settings}>
                {this.state.list.map((item, index) => {
                  return (
                    <div key={index}>
                      <div>
                        <img
                          src={item}
                          className="img-fluid m-auto"
                          alt="Brand"
                          width="120"
                        />
                      </div>
                    </div>
                  );
                })}
              </Slider>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
