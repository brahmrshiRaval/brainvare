import React, { Component } from "react";

import { Helmet } from "react-helmet";
import { Container, Row, Col } from "reactstrap";
// import "./homepage.scss";
import "./Logoslider/slider.scss";
import "./index.scss";
import Banner from "./Banner/index";
import Sidebar from "./Sidlebar/index";
import LogoSlider from "./Logoslider/index";
export default class extends Component {
  scrollPositions = {};
  componentWillMount() {
    if ("scrollRestoration" in window.history) {
      window.history.scrollRestoration = "manual";
    }
    window.scrollTo(0, 0);
  }
  checkPrice = () => {
    this.props.history.push("/service");
  };

  render() {
    return (
      <div className="backgound-color-set">
        <Helmet>
          <title></title>
          <meta name="description" content="" />
          <meta property="og:title" content="" />
          <meta property="og:description" content="" />
          <meta name="twitter:description" content="" />
          <meta name="twitter:title" content="" />
          <link rel="canonical" href="" />
        </Helmet>
        <Container fluid={true}>
          <Row>
            <Col md={4} lg={4} sm={12} xs={12}>
              <Sidebar />
            </Col>
            <Col md={8} lg={8} sm={12} xs={12} className="p-0">
              <Banner />
            </Col>
          </Row>
        </Container>
        <Container fluid={true} className="p-0">
          <Row className="justify-content-center section-brands-slider">
            <LogoSlider />
          </Row>
        </Container>
      </div>
    );
  }
}
