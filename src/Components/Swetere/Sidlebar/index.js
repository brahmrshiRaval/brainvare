import React, { Component } from "react";
import {
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
  Typography,
  // ExpandMoreIcon,
} from "@material-ui/core";
import { Container, Row, Col } from "reactstrap";
export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: 0,
    };
  }
  expandPanelChange = (panel, value) => (event, expanded) => {
    // debugger;
    this.setState({
      expanded: expanded ? panel : null,
      image: expanded ? true : false,
    });
  };
  render() {
    return (
      <div className="sidebar">
        <Container>
          <h2>
            <b>SWATERS</b>
          </h2>
          <div className="expantionpanel-style">
            <ExpansionPanel
              expanded={this.state.expanded === "faqpanel1"}
              onChange={this.expandPanelChange("faqpanel1")}
            >
              <ExpansionPanelSummary
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <Typography className="accordion-toggle">
                  <b>ARAN JUMPER</b>
                </Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <Typography>
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry. Lorem Ipsum has been the industry's
                  standard dummy text ever since the 1500s, when an unknown
                  printer took a galley of type and scrambled it to make a type
                  specimen book. It has survived not only five centuries, but
                  also the leap into electronic typesetting, remaining
                  essentially unchanged.
                </Typography>
              </ExpansionPanelDetails>
            </ExpansionPanel>
          </div>
          <div className="expantionpanel-style">
            <ExpansionPanel
              expanded={this.state.expanded === "faqpanel2"}
              onChange={this.expandPanelChange("faqpanel2")}
            >
              <ExpansionPanelSummary
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <Typography className="accordion-toggle">
                  <b>CHRISTMAS JUMPER</b>
                </Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <Typography>
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry. Lorem Ipsum has been the industry's
                  standard dummy text ever since the 1500s, when an unknown
                  printer took a galley of type and scrambled it to make a type
                  specimen book. It has survived not only five centuries, but
                  also the leap into electronic typesetting, remaining
                  essentially unchanged.
                </Typography>
              </ExpansionPanelDetails>
            </ExpansionPanel>
          </div>
          <div className="expantionpanel-style">
            <ExpansionPanel
              expanded={this.state.expanded === "faqpanel3"}
              onChange={this.expandPanelChange("faqpanel3")}
            >
              <ExpansionPanelSummary
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <Typography className="accordion-toggle">
                  <b>PENGUIN SWEATER</b>
                </Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <Typography>
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry. Lorem Ipsum has been the industry's
                  standard dummy text ever since the 1500s, when an unknown
                  printer took a galley of type and scrambled it to make a type
                  specimen book. It has survived not only five centuries, but
                  also the leap into electronic typesetting, remaining
                  essentially unchanged.
                </Typography>
              </ExpansionPanelDetails>
            </ExpansionPanel>
          </div>
          <div className="expantionpanel-style">
            <ExpansionPanel
              expanded={this.state.expanded === "faqpanel4"}
              onChange={this.expandPanelChange("faqpanel4")}
            >
              <ExpansionPanelSummary
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <Typography className="accordion-toggle">
                  <b>COWICHAN KNITTING</b>
                </Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <Typography>
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry. Lorem Ipsum has been the industry's
                  standard dummy text ever since the 1500s, when an unknown
                  printer took a galley of type and scrambled it to make a type
                  specimen book. It has survived not only five centuries, but
                  also the leap into electronic typesetting, remaining
                  essentially unchanged.
                </Typography>
              </ExpansionPanelDetails>
            </ExpansionPanel>
          </div>
          <div className="expantionpanel-style">
            <ExpansionPanel
              expanded={this.state.expanded === "faqpanel5"}
              onChange={this.expandPanelChange("faqpanel5")}
            >
              <ExpansionPanelSummary
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <Typography className="accordion-toggle">
                  <b>HOODIE</b>
                </Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <Typography>
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry. Lorem Ipsum has been the industry's
                  standard dummy text ever since the 1500s, when an unknown
                  printer took a galley of type and scrambled it to make a type
                  specimen book. It has survived not only five centuries, but
                  also the leap into electronic typesetting, remaining
                  essentially unchanged.
                </Typography>
              </ExpansionPanelDetails>
            </ExpansionPanel>
          </div>
        </Container>
        <Container>
          <div className="widget">
            <h2>
              <b>size filter</b>
            </h2>
            <ul>
              <li>
                <a href="#">
                  xs <span>(2)</span>
                </a>
              </li>
              <li>
                <a href="#">
                  s <span>(1)</span>
                </a>
              </li>
              <li>
                <a href="#">
                  m <span>(2)</span>
                </a>
              </li>
              <li>
                <a href="#">
                  l <span>(4)</span>
                </a>
              </li>
              <li>
                <a href="#">
                  xl <span>(3)</span>
                </a>
              </li>
              <li>
                <a href="#">
                  xxl <span>(1)</span>
                </a>
              </li>
            </ul>
          </div>
        </Container>
      </div>
    );
  }
}
