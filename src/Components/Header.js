import React, { Component } from "react";
import logo from "../Images/logo-big.png";
import { NavItem, Container, Row, Col } from "reactstrap";
import { userSignOut } from "../actiions/index";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";

class Navigaion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      isCartHasDoorStepService: false,
      token: "",
      scrolled: false,
      city: [],
      selected: {
        value: "",
        lable: "",
      },
    };
  }
  render() {
    return (
      <div className={`header`}>
        <div className="laguage">
          <Container>
            <Row>
              <Col md={6} sm={6} xs={6}>
                <ul className="inline">
                  <li className="active">
                    <NavLink to="/">eng</NavLink>
                  </li>
                  <li>
                    <NavLink to="/">de</NavLink>
                  </li>
                  <li>
                    <NavLink to="/">pl</NavLink>
                  </li>
                </ul>
              </Col>
              <Col xs={6} className=" text-right">
                <ul className="inline">
                  <li className="active">
                    <NavLink to="/">us</NavLink>
                  </li>
                  <li>
                    <NavLink to="/">eur</NavLink>
                  </li>
                  <li>
                    <NavLink to="/">pln</NavLink>
                  </li>
                </ul>
              </Col>
            </Row>
          </Container>
        </div>
        <Container>
          <section className="style-one-header top-area">
            <Row>
              <Col sm={4} xs={12}>
                <div className="login-menu-holder ic-sm-user">
                  welcome, you can
                  <NavLink to="/"> login</NavLink> or
                  <NavLink to="/"> register</NavLink>
                </div>
                <div className="hotline-holder ic-sm-phone">
                  <label>hotline:</label>
                  <span>+88 987 654 321</span>
                </div>
              </Col>
              <Col sm={4} xs={12} className="top-logo-holder">
                <div className="top-logo">
                  <NavLink to="/">
                    <img alt="LOGO" src={logo} />
                  </NavLink>
                </div>
              </Col>
              <Col sm={4} xs={12}>
                <div className="wish-cart-holder ">
                  <div className="wishlist-holder ic-sm-heart">
                    wishlist:
                    <span>2</span>
                  </div>
                  {/* <div className="top-cart-holder ">
                    shoping cart:
                    <span className="top-cart-price">$129.00</span>
                    <div className="total-buble">
                      <span>10</span>
                    </div>
                    <div className="hover-holder">
                      <ul className="basket-items ">
                        <li className="row">
                          <div className="thumb col-xs-3">
                            <img
                              width="45"
                              height="45"
                              alt=""
                              src="images/products/product02-sq.jpg"
                            />
                          </div>
                          <div className="body col-xs-9">
                            <h5>fashion bag</h5>
                            <div className="price">
                              <span>$189.00</span>
                            </div>

                            <NavLink className="remove-item" href="#close">
                              x
                            </NavLink>
                          </div>
                        </li>

                        <li className="row">
                          <div className="thumb col-xs-3">
                            <img
                              width="45"
                              height="45"
                              alt=""
                              src="images/products/product02-sq.jpg"
                            />
                          </div>
                          <div className="body col-xs-9">
                            <h5>fashion bag</h5>
                            <div className="price">
                              <span>$189.00</span>
                            </div>

                            <NavLink className="remove-item" href="#close">
                              x
                            </NavLink>
                          </div>
                        </li>

                        <li className="row">
                          <div className="thumb col-xs-3">
                            <img
                              width="45"
                              height="45"
                              alt=""
                              src="images/products/product02-sq.jpg"
                            />
                          </div>
                          <div className="body col-xs-9">
                            <h5>fashion bag</h5>
                            <div className="price">
                              <span>$189.00</span>
                            </div>

                            <NavLink className="remove-item" href="#close">
                              x
                            </NavLink>
                          </div>
                        </li>
                      </ul>

                      <NavLink className="top-chk-out md-button" href="checkout.html">
                        check out
                      </NavLink>
                    </div>
                  </div> */}
                  <div className="search-holder">
                    <form>
                      <input type="text" placeholder="SEARCH" />
                    </form>
                  </div>
                </div>
              </Col>
            </Row>
          </section>
        </Container>
        <div class="top-menu visible-md visible-lg">
          <ul>
            <li>
              <NavLink to="/">Jacket</NavLink>
            </li>
            <li>
              <NavLink to="/sweter">Sweteres</NavLink>
            </li>
            <li>
              <NavLink to="/">dresses</NavLink>
            </li>
            <li>
              <NavLink to="/">skirts</NavLink>
            </li>
            <li>
              <NavLink to="/">pants</NavLink>
            </li>
            <li>
              <NavLink to="/">shorts</NavLink>
            </li>
            <li>
              <NavLink to="/">shoes</NavLink>
            </li>
            <li>
              <NavLink to="/">accessories</NavLink>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state, response) => {
  debugger;
  return {
    itemCount: state.form.Cart?.cart?.length || 0,
  };
};
const mapDispatchToProps = (dispatch) => {
  debugger;
  return {
    // dispatching plain actions
    userSignOut: userSignOut,
    signOut: () => dispatch({ type: "USER_SIGN_OUT" }),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Navigaion);
