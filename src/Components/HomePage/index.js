import React, { Component } from "react";

import { Helmet } from "react-helmet";
import { Container } from "reactstrap";
// import "./homepage.scss";
import Banner from "./Banner/index";
import Blog from "./FromBlog/index";
import Nwesletter from "./Newsletter/index";
export default class extends Component {
  scrollPositions = {};
  componentWillMount() {
    if ("scrollRestoration" in window.history) {
      window.history.scrollRestoration = "manual";
    }
    window.scrollTo(0, 0);
  }
  checkPrice = () => {
    this.props.history.push("/service");
  };

  render() {
    return (
      <div className="backgound-color-set">
        <Helmet>
          <title></title>
          <meta name="description" content="" />
          <meta property="og:title" content="" />
          <meta property="og:description" content="" />
          <meta name="twitter:description" content="" />
          <meta name="twitter:title" content="" />
          <link rel="canonical" href="" />
        </Helmet>
        <Container fluid={true}>
          <Banner />
        </Container>
        <Container fluid={true}>
          <Nwesletter />
        </Container>
        <Container fluid={true}>
          <Blog />{" "}
        </Container>
      </div>
    );
  }
}
