import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
export default class index extends Component {
  render() {
    return (
      <div>
        <section className="section-from-out-blog">
          <Container>
            <h2>from out blog</h2>
            <Row className=" items-holder">
              <Col xs={6}>
                <div className="from-blog-item">
                  <Row>
                    <Col xs={12} sm={5} lg={3}>
                      <div class="thumb">
                        <img
                          className="lazy"
                          alt=""
                          src="http://justadomain.xyz/demo/modello/images/post-thumb02.png"
                        />
                      </div>
                    </Col>
                    <Col xs={12} sm={7} lg={9}>
                      <div className="body">
                        <h4>
                          <b>
                            <a href="#">big sales coming soon</a>
                          </b>
                        </h4>
                        <span className="date">15, jan 2014</span>
                        <p className="excerpt">
                          Lorem ipsum dolor sit amet, consectetur adipiscing
                          elit. Vivamus tempor at enim non consequat...
                        </p>

                        <div className="comment">
                          <a href="#">
                            <span>2</span> comments
                          </a>
                        </div>
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col xs={6}>
                <div className="from-blog-item">
                  <div className="row">
                    <div className="col-xs-12 col-sm-5 col-lg-3">
                      <div className="thumb">
                        <img
                          className="lazy"
                          alt=""
                          src="http://justadomain.xyz/demo/modello/images/post-thumb02.png"
                        />
                      </div>
                    </div>
                    <div className="col-xs-12 col-sm-7 col-lg-9">
                      <div className="body">
                        <h4>
                          <b>
                            <a href="#">big sales coming soon</a>
                          </b>
                        </h4>
                        <span className="date">15, jan 2014</span>
                        <p className="excerpt">
                          Lorem ipsum dolor sit amet, consectetur adipiscing
                          elit. Vivamus tempor at enim non consequat...
                        </p>

                        <div className="comment">
                          <a href="#">
                            <span>2</span> comments
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </Col>
            </Row>
          </Container>
        </section>
      </div>
    );
  }
}
