import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
export default class index extends Component {
  render() {
    return (
      <div>
        <section className="section-newsletter">
          <Container>
            <div className="news-letter-holder">
              <form>
                <Row>
                  <Col xs={12} sm={4} className="newsletter-title ">
                    <h2>sign up to our newsletter</h2>
                    <h3>and get $30 coupon</h3>
                  </Col>
                  <Col xs={12} sm={8}>
                    <div className="newsletter-body">
                      <input
                        className="col-xs-12 col-sm-7"
                        placeholder="Enter here your email"
                      />
                      <button>sign up</button>
                    </div>
                  </Col>
                </Row>
              </form>
            </div>
          </Container>
        </section>
        <section className="section-banners">
          <Container fluid={true}>
            <Row>
              <Col lg={6} md={12}>
                <div className="homepage-banner">
                  <a href="#">
                    <img
                      className="img-fluild"
                      width="584"
                      height="211"
                      alt="Sale"
                      src="http://justadomain.xyz/demo/modello/images/banner-freeshipping.jpg"
                    />
                  </a>
                </div>
              </Col>
              <Col lg={6} md={12}>
                <div className="homepage-banner">
                  <a href="#">
                    <img
                      className="img-fluild"
                      width="584"
                      height="211"
                      alt="Sale"
                      src="http://justadomain.xyz/demo/modello/images/banner-freeshipping.jpg"
                    />
                  </a>
                </div>
              </Col>
            </Row>
          </Container>
        </section>
      </div>
    );
  }
}
