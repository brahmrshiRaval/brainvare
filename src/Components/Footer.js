import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
// import { Facebook, Twitter, Instagram } from "@material-ui/icons";
import logo from "../Images/logo-small.png";
import p6 from "../Images/product06-sq.jpg";
import p5 from "../Images/product05-sq.jpg";
import p4 from "../Images/product04-sq.jpg";
import paymentspaypal from "../Images/payments-paypal.png";
import paymentsdiscover from "../Images/payments-discover.png";
import paymentsmastercard from "../Images/payments-mastercard.png";
import paymentsskrill from "../Images/payments-skrill.png";
import paymentsvisa from "../Images/payments-visa.png";
// import FacebookIcon from "@material-ui/icons/Facebook";
export default class Footer extends Component {
  render() {
    return (
      <div className="footer">
        <Container fluid={true}>
          <section class="section-footer">
            <div class="footer-logo-holder">
              <img src={logo} alt="Modello-Logo" />
            </div>
            <div class="footer-holder">
              <Container>
                <Row>
                  <Col
                    xs={8}
                    sm={6}
                    md={3}
                    class="col-xs-offset-2  col-sm-offset-0"
                  >
                    <div class="footer-column adress-column">
                      <h4>connect with us</h4>

                      <div class="content">
                        <p class="bold">Modelo - eCommerce</p>
                        <p>
                          4 East 80th Street, new york,ny
                          <br />
                          +88 897 454 321 <br />
                          say@hello.com
                        </p>
                        <div class="footer-socials">
                          <ul>
                            <li>
                              <a href="#" class="fa fa-facebook"></a>
                            </li>

                            <li>
                              <a href="#" class="fa fa-twitter"></a>
                            </li>
                            <li>
                              <a href="#" class="fa fa-dribbble"></a>
                            </li>
                            <li>
                              <a href="#" class="fa fa-google-plus"></a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </Col>
                  <Col
                    xs={8}
                    sm={6}
                    md={3}
                    class="col-xs-offset-2  col-sm-offset-0"
                  >
                    <div class="footer-column">
                      <h4>holıday sales</h4>
                      <div class="content">
                        <ul class="footer-products">
                          <li class="row">
                            <div class="thumb col-xs-3">
                              <a href="#">
                                <img alt="product06-sq" src={p6} />
                              </a>
                            </div>
                            <div class="body col-xs-9">
                              <h5>
                                <a href="#">calvin clane - slim pants</a>
                              </h5>
                              <div class="price">
                                <span class="previous-price">$220.00</span>
                                <span>$189.00</span>
                              </div>
                            </div>
                          </li>
                          <li class="row">
                            <div class="thumb col-xs-3">
                              <a href="#">
                                <img alt="product06-sq" src={p6} />
                              </a>
                            </div>
                            <div class="body col-xs-9">
                              <h5>
                                <a href="#">calvin clane - slim pants</a>
                              </h5>
                              <div class="price">
                                <span class="previous-price">$220.00</span>
                                <span>$189.00</span>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </Col>
                  <div class="col-xs-offset-2 col-xs-8 col-sm-6 col-sm-offset-0 col-md-3">
                    <div class="footer-column">
                      <h4>new arrivals</h4>

                      <div class="content">
                        <ul class="footer-products">
                          <li class="row">
                            <div class="thumb col-xs-3">
                              <a href="#">
                                <img alt="product05-sq" src={p5} />
                              </a>
                            </div>
                            <div class="body col-xs-9">
                              <h5>
                                <a href="#">calvin clanes - low slim jeans</a>
                              </h5>
                              <div class="price">
                                <span class="previous-price">$220.00</span>
                                <span>$189.00</span>
                              </div>
                            </div>
                          </li>

                          <li class="row">
                            <div class="thumb col-xs-3">
                              <a href="#">
                                <img alt="product04-sq" src={p4} />
                              </a>
                            </div>
                            <div class="body col-xs-9">
                              <h5>
                                <a href="#">mc neal - gummi sweater</a>
                              </h5>
                              <div class="price">
                                <span class="previous-price">$220.00</span>
                                <span>$189.00</span>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <Col
                    xs={8}
                    sm={6}
                    md={3}
                    class="col-xs-offset-2 col-sm-offset-0 "
                  >
                    <div class="footer-column">
                      <h4>our support</h4>
                      <div class="content">
                        <ul class="link-list">
                          <li>
                            <a href="">terms & conditions</a>
                          </li>

                          <li>
                            <a href="">delivery</a>
                          </li>
                          <li>
                            <a href="">secure payment</a>
                          </li>

                          <li>
                            <a href="">contact us</a>
                          </li>

                          <li>
                            <a href="">refunds</a>
                          </li>

                          <li>
                            <a href="">track orders</a>
                          </li>
                          <li>
                            <a href="">services</a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </Col>
                </Row>
              </Container>
            </div>
            <div class="footer-payment-icons">
              <ul class="list-inline">
                <li>
                  <img alt="paypal" src={paymentspaypal} />
                </li>
                <li>
                  <img alt="visa" src={paymentsvisa} />
                </li>
                <li>
                  <img alt="master card" src={paymentsmastercard} />
                </li>
                <li>
                  <img alt="discover" src={paymentsdiscover} />
                </li>
                <li>
                  <img alt="skrill" src={paymentsskrill} />
                </li>
              </ul>
            </div>
          </section>
        </Container>
      </div>
    );
  }
}
