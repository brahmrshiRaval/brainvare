import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
// import App from "./App";
import * as serviceWorker from "./serviceWorker";
import "bootstrap/dist/css/bootstrap.min.css";
// import "slick-carousel/slick/slick.css";
// import "slick-carousel/slick/slick-theme.css";
import Route from "./Router/index";
import { history } from "./utils/History";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/lib/integration/react";
// import "react-perfect-scrollbar/dist/css/styles.css";
import configureStore from "./utils/Store";
const { store, persistor } = configureStore();
// import { browserHistory } from 'react-router';
ReactDOM.render(
  <Provider store={store}>
    <PersistGate persistor={persistor}>
      <Route history={history} />
    </PersistGate>
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
