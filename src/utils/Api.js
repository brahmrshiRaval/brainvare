import axios from "axios";
import { AppSettings } from "./AppSetting";
import { getSession } from "./Session";
import { userSignOut } from "../actiions/index";
import configureStore from "./Store";
import { history } from "./History";
// import { withRouter } from "react-router";
// import { useDispatch } from "react-redux";
const { store } = configureStore();
export const axiosInstance = () => {
  const instance = axios.create({
    baseURL: AppSettings.API_BASE_URL,
    headers: {
      "Content-Type": "application/json",
    },
  });
  instance.interceptors.request.use(
    (config) => {
      var token = getSession("token");
      // var token = "fa5f9132de84291835f32cd785f7de56KRH3LxrL2ZW4EzVep5SKasl0VKF1KptfRF9LOjOQLQi5R4JRcoTdAd/qz4bKUX5X";
      config.headers["Authorization"] = "Bearer " + token;
      return config;
    },
    (error) => {
      return Promise.reject(error);
    }
  );

  instance.interceptors.response.use(
    (response) => {
      debugger;
      if (response.data.status !== -1) {
        return response;
      } else {
        history.push("/login");
        window.location.href = "/login";
        store.dispatch(userSignOut());
        store.dispatch({ type: "USER_SIGN_OUT" });
        return response;
      }
    },
    (error) => {
      // console.log(toString(error));
      debugger;
      if (error.Error === undefined) {
        // console.log("----SESSION----");
        // console.log(error);
        history.push("/login");
        window.location.href = "/login";
        store.dispatch(userSignOut());
        store.dispatch({ type: "USER_SIGN_OUT" });
      } else if (
        error.response.data.status === -1 &&
        error.response.data.message === "Invalid Token"
      ) {
        // console.log("----SESSION----");
        // console.log(error);
        history.push("/login");
        window.location.href = "/login";
        store.dispatch(userSignOut());
        store.dispatch({ type: "USER_SIGN_OUT" });
      } else {
        history.push("/login");
        window.location.href = "/login";
        store.dispatch(userSignOut());
        store.dispatch({ type: "USER_SIGN_OUT" });
        // const msg = error.response
        //   ? error.response.data.message
        //   : error.message;
      }
      return Promise.reject(error);
    }
  );

  return instance;
};
