import Ws from "@adonisjs/websocket-client";
import { AppSettings } from "./AppSetting";
// import { getSession } from "./Session";
export const ws = Ws(`${AppSettings.socket_url}`, {
  reconnectionAttempts: 5,
});
