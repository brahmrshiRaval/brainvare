const INIT_STATE = {
  cart: [],
  service: [],
  conformorder: [],
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case "SET_CAR": {
      debugger;
      return { ...state, car: action.payload, cart: [] };
    }
    case "ADD_CART": {
      debugger;
      return { ...state, cart: action.payload };
    }
    case "DELETE_ITEM": {
      debugger;
      return { ...state, cart: action.payload };
    }
    case "CLEAR_CART": {
      debugger;
      return { ...state, cart: [], conformorder: action.payload };
    }
    case "serviceUpdate": {
      debugger;
      return { ...state, service: action.payload };
    }
    default:
      return state;
  }
};
