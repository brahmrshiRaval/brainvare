import { combineReducers } from 'redux';
import Auth from './Auth';
import Cart from './cart';
export default combineReducers({
  Auth,
  Cart,
});
