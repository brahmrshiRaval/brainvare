export const INIT_URL = "init_url";
export const SIGNOUT_USER_SUCCESS = "signout_user_success";
export const USER_DATA = "user_data";
export const USER_TOKEN_SET = "user_token_set";
export const FETCH_ERROR = "fetch_error";
export const FETCH_START = "fetch_start";
export const FETCH_SUCCESS = "fetch_success";
