import {
  FETCH_ERROR,
  FETCH_START,
  FETCH_SUCCESS,
  SIGNOUT_USER_SUCCESS,
  USER_DATA,
  USER_TOKEN_SET,
} from "./constant";
import { axiosInstance } from "../utils/Api";
import { removeSession } from "../utils/Session";

export const userSignIn = (value) => {
  debugger;
  return (dispatch) => {
    dispatch({ type: FETCH_START });
    var formdata = new FormData();
    formdata.append("user_mobile", value);
    axiosInstance()
      .post("web/auth/login", formdata)
      .then(({ data }) => {
        if (data.status === 1) {
          // setSession(data.data);
          dispatch({ type: FETCH_SUCCESS });
          dispatch({ type: USER_TOKEN_SET, payload: data.data.api_token });
          dispatch({ type: USER_DATA, payload: data.data });
        } else {
          dispatch({ type: FETCH_ERROR, payload: data.error });
        }
      })
      .catch(function (error) {
        dispatch({ type: FETCH_ERROR, payload: "" });
      });
  };
};
export const userSignOut = () => {
  return (dispatch) => {
    dispatch({ type: FETCH_START });
    // axios.post('auth/logout',
    // ).then(({data}) => {
    //   if (data.result) {

    dispatch({ type: FETCH_SUCCESS });
    dispatch({ type: "USER_SIGN_OUT" });
    removeSession();
    dispatch({ type: FETCH_SUCCESS });
    dispatch({ type: SIGNOUT_USER_SUCCESS });
    //   } else {
    //     dispatch({type: FETCH_ERROR, payload: data.error});
    //   }
    // }).catch(function (error) {
    //   dispatch({type: FETCH_ERROR, payload: error.message});
    //   console.log("Error****:", error.message);
    // });
  };
};
