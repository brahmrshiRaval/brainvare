import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
// import { history } from "../utils/History";
import Header from "../Components/Header";
import Footer from "../Components/Footer";
import Homepage from "../Components/HomePage";
import Swetere from "../Components/Swetere/index";
// const PrivateRoute = ({ component: Component, ...rest }) => {
//   const user = getSession("token");
//   return (
//     <Route
//       {...rest}
//       render={(props) =>
//         user ? (
//           <Component user={user} {...props} />
//         ) : (
//           <Route path="/login" component={LoginPage} />
//         )
//       }
//     />
//   );
// };
export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
    };
  }
  render() {
    return (
      <Router>
        <div>
          <Header />
          <Switch>
            <Route exact path="/" component={Homepage} />
            <Route exact path="/sweter" component={Swetere} />
            <Route component={Homepage} />
          </Switch>
          <Footer />
        </div>
      </Router>
    );
  }
}
